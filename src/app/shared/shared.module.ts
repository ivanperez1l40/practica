import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { LottieModule } from 'ngx-lottie';

import { PaginatorComponent } from '../componentes/paginator/paginator.component';
import { NgxGlideModule } from 'ngx-glide';


@NgModule({
  declarations: [
     PaginatorComponent
  ],
  imports: [
    CommonModule,
    LottieModule,
    NgxGlideModule,
    
  
  ],
  exports:[
     PaginatorComponent,
    LottieModule,
    NgxGlideModule,
   
    
  ]
})
export class SharedModule {
  static forRoot(): any[] | import("@angular/core").Type<any> | import("@angular/core").ModuleWithProviders<{}> {
    throw new Error('Method not implemented.');
  }
}
