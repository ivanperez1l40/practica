import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { MainComponent } from './pages/main/main.component';
import { ActualizarComponent } from './pages/actualizar/actualizar.component';
import { RegistrarPostComponent } from './pages/registrar-post/registrar-post.component';





@NgModule({
  declarations: [
    LoginComponent,
    RegistroComponent,
    MainComponent,
    ActualizarComponent,
    RegistrarPostComponent,

   
  ],
  imports: [

    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule
  ]
})
export class AuthModule { }
