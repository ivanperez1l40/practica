import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/servicios/auth.service';
@Component({
  selector: 'app-actualizar',
  templateUrl: './actualizar.component.html',
  styleUrls: ['./actualizar.component.css']
})
export class ActualizarComponent {


  miFormulario!: FormGroup;
  id: any
constructor(private servicio: AuthService, private fb:FormBuilder) {
  this.miFormulario = this.fb.group({
    email: ['', [ Validators.required, Validators.email] ],
    password: ['', [ Validators.required, Validators.minLength(6)] ],
    name: ['',[Validators.required]],
    username:['',[Validators.required]]
  });
  const iduser = localStorage.getItem('id');
  this.id = iduser
  console.log(iduser)
  this.infouser(iduser)
  
}
// variables para editar usuario

datos: any
infouser(userToken: string | null){
  this.servicio.buscarporiduser(userToken).subscribe(resp => {
  console.log(resp)
  this.datos = resp
  let user = this.datos[0]
      
         this.miFormulario.patchValue(user)

  })
 
}

actualizar(){
  const {email, password,name, username} = this.miFormulario.value
  this.servicio.actualizardatos( this.id,email,password,name,username).subscribe(resp => {
    console.log(resp)
  })
}

}
