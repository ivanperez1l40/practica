import { Component } from '@angular/core';
import { FormGroup, Validators,FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/servicios/auth.service';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent {
  miFormulario: FormGroup = this.fb.group({
    email: ['', [ Validators.required, Validators.email] ],
    password: ['', [ Validators.required, Validators.minLength(6)] ],
    name: ['',[Validators.required]],
    username:['',[Validators.required]]
  });
 constructor(private fb:FormBuilder, private route: Router, private servicio : AuthService){

 }

 registro(){

  const {email,password,name,username} = this.miFormulario.value
  this.servicio.registro(email,password,name,username).subscribe(resp => {
   console.log(resp)
   
  })
   this.route.navigateByUrl('/auth/login')
 }

}