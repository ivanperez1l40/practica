import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { PostService } from 'src/app/servicios/post.service';

@Component({
  selector: 'app-registrar-post',
  templateUrl: './registrar-post.component.html',
  styleUrls: ['./registrar-post.component.css']
})
export class RegistrarPostComponent {
  miFormulario: FormGroup = this.fb.group({
    title: ['',[Validators.required]],
    body:['',[Validators.required]]
  });
id: any
constructor(private fb:FormBuilder, private servicio:PostService){
  const iduser = localStorage.getItem('id');
  this.id = iduser
  console.log(iduser)
  
}

registropost(){
  const {title,body} = this.miFormulario.value
   this.servicio.registro(title,body,this.id).subscribe(resp => {

   console.log(resp)

   })
}
}
