import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarPostComponent } from './registrar-post.component';

describe('RegistrarPostComponent', () => {
  let component: RegistrarPostComponent;
  let fixture: ComponentFixture<RegistrarPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarPostComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistrarPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
