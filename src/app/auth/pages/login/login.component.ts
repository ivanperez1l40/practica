import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/servicios/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
miFormulario: FormGroup = this.fb.group({
  email: ['', [ Validators.required, Validators.email] ],
  password: ['', [ Validators.required, Validators.minLength(6)] ]
  
});
token: any
id: any
datos: any


  constructor(private fb:FormBuilder, private servicio: AuthService, private router: Router){
  }

  login() {
  const {email, password} = this.miFormulario.value
  this.servicio.login(email, password).subscribe( resp => {
  this.datos = resp
  

   if(this.datos[0].email == email && this.datos[0].password == password ){
     console.log("datos estan correctos"),
   
     this.token = `${this.datos[0].name}8${this.datos[0].email}`
     this.id = this.datos[0].id
     this.saveToken(this.token,this.id)
     Swal.fire('Success',"Bienvenido")
     this.router.navigateByUrl('/dashboard');
     
   
     this.servicio.disparadorid.emit({
      data: this.datos[0].id
    })


     
   }else
   {
    
    Swal.fire('Error',"los datos estan incorrectos")
   }
   
  
 

  } )
  //  this.router.navigateByUrl('/dashboard')
  }
  logout(): void{
    
    localStorage.removeItem("ACCESS_TOKEN");
    localStorage.removeItem("EXPIRES_IN");
    localStorage.removeItem("rol");
    this.router.navigate(['/login'])
  
  }

  private saveToken(token:string, id: string  ): void {
    localStorage.setItem("ACCESS_TOKEN", token);
    localStorage.setItem("id", id );
    // this.token = token;
    // this.loggedId.next(false);
    // this.roles.next(false)
  }

}
