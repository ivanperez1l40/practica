import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { LoginComponent } from './pages/login/login.component';
import { RegistroComponent } from './pages/registro/registro.component';

import { ActualizarComponent } from './pages/actualizar/actualizar.component';
import { RegistrarPostComponent } from './pages/registrar-post/registrar-post.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent, 
    children: [
      {path: 'login', component: LoginComponent},
      {path: 'registro' ,  component:RegistroComponent},
      {path: 'actualizar', component:ActualizarComponent},
      {path: 'registro-post', component:RegistrarPostComponent},
      {path :  '**', redirectTo: 'login'}
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
