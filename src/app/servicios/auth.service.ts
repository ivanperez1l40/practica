import { EventEmitter, Injectable, Output } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../enviroments/enviroment';
import { infoUser, User } from '../auth/interfaces/users';
import { catchError, map, tap, throwError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

 private baseUrl: string = environment.baseUrl
 private _user!: infoUser
  datoss:any

  @Output() disparadorid: EventEmitter<any> = new EventEmitter();
  userData: any;
 
  constructor(private http: HttpClient) { }
  


  private handleError(error: HttpErrorResponse){
    if(error.error instanceof ErrorEvent){
      console.error(error.error.message);
    }

    else{
      console.error(`Error status: ${error.status}, error: ${error.error}`);
    }
    return throwError( 'Hubo un error en la aplicacion. Verificar logs');
  }

  login(email: string, password: string) {
    const url = `${this.baseUrl}users`;
    const body = {email, password};
     return this.http.get<User>(`${url}?email=${body.email}`).pipe(
      map( resp => resp
        ), catchError(this.handleError)
     )
    
  }

  registro(email: string, password: string, name:string, username:string){
  
    const url = `${this.baseUrl}users`;
    const body = {email, password,name,username};
   
    return this.http.post(url, body)
  }

  revisarsiexiste(email: string) {
    const url = `${this.baseUrl}users`;
    const body = {email,};
    return this.http.get(`${url}?email=${body.email}`).pipe(
      map( resp => resp
        ),catchError(this.handleError)
     )

  }

  buscarporiduser(id: any) {
    const url = `${this.baseUrl}users`;
    const body = {id,};
    return this.http.get(`${url}?id=${body.id}`).pipe(
      map( resp => resp
        ), catchError(this.handleError)
     )

  }

  actualizardatos(userToken:any, email: string,password: string,name: string,username:string){

    const url = `${this.baseUrl}users`;
    const body = {userToken,email,password,name,username};
    
    return this.http.put(`${url}/${body.userToken}`, body)
  }
}
