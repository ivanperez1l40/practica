import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../enviroments/enviroment';
import { catchError, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private baseUrl: string = environment.baseUrl
  constructor(private http: HttpClient) { }


  registro(title:string, body:string, userId: any){
  
    const url = `${this.baseUrl}post`;
    const bodyp = {title, body,userId};
    return this.http.post(url, bodyp)
  }

  extraerposts(id: string) {
    const url = `${this.baseUrl}post`;
    const body = {id};
    return this.http.get(`${url}?userId=${body.id}`).pipe(
      map( resp => resp
        )

     )

  }
  handleError(handleError: any): import("rxjs").OperatorFunction<Object, any> {
    throw new Error('Method not implemented.');
  }


}
