import { EventEmitter, Injectable, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class RequestService {

  @Output() disparadorinfo: EventEmitter<any> = new EventEmitter();

  ida: any
  
  constructor(private http: HttpClient) {
   this.disparadorinfo.subscribe(data => 
    this.ida = data)
   
   }

  get(url: string, params = {}, headers = {}): Observable<any> {
    console.log(url)
    return this.http.get(url, {
      
      params: {
        ...params
      },
      headers: {
        ...headers
      }
    });
    
  }

  getpost(url: string, params = {}, headers = {}): Observable<any> {
    console.log(url)
    return this.http.get(url, {
      
      params: {
        ...params
      },
      headers: {
        ...headers
      }
    });
    
  }
  
  getalbum(url: string, params = {}, headers = {}): Observable<any> {
   
    return this.http.get(url, {
      
      params: {
        ...params
      },
      headers: {
        ...headers
      }
    });
    
  }

  update(url: string, body: any): Observable<any> {
    return this.http.put(url, body);
  }

  post(url: string, body: any): Observable<any> {
    return this.http.post(url, body);
  }

  delete(url: string): Observable<any> {
    return this.http.delete(url);
  }
}
