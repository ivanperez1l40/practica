import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InterceptorService } from './Interceptores/interceptor.service';


import { LottieModule } from 'ngx-lottie';
import { LayoutComponent } from './layout/layout/layout.component';
import { LayoutModule } from './layout/layout/layout.module';




export function playerFactory(): any {
  return import('lottie-web');
}

@NgModule({
  declarations: [
    AppComponent,
   
   
  
  ],
  imports:[
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LottieModule.forRoot({
      player: playerFactory
    }),
    LayoutModule
    
  
  
    
    
     
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
