import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProtectedRoutingModule } from './protected-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { PostComponent } from './dashboard/post/post.component';
import { PostItemModule } from './dashboard/post/post-item/post-item.module';





@NgModule({
  declarations: [
    DashboardComponent,
    PostComponent,
  ],
  imports: [
    CommonModule,
    ProtectedRoutingModule,
    SharedModule,
    PostItemModule,
    
  
  ]
})
export class ProtectedModule { }
