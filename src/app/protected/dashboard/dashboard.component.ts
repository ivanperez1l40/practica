import { Component } from '@angular/core';
import { Route, Router } from '@angular/router';
import { AuthService } from 'src/app/servicios/auth.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
id: any
User : String | undefined
  constructor(private router: Router, private servicio: AuthService) {  
  
   this.servicio.disparadorid.subscribe(data => {

    this.User = data.data
    console.log(this.User)  
   })
   


  }


  
  actualizar(){
 
    this.router.navigate(['/auth/actualizar'] )
    
  }



  logout(){
  this.router.navigate(['/auth'])
  }
  registrarpost(){
    this.router.navigate(['/auth/registro-post'])
  }
}
