import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { firstValueFrom, forkJoin } from 'rxjs';
import { RequestService } from 'src/app/servicios/request.service';
import { FormBuilder, FormGroup } from '@angular/forms';
declare const bootstrap: any;
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent {
  

  currentTip!: any
  lastestTips: any; 
  lastcomment: any
  @Input()
  item: any
  data: any = ""
  tipsQty: number | undefined;
  modalinfo: any;
  loadingExtraInfo = false;
  @ViewChild('modalInfo', { static: true }) modalInfo!: ElementRef<HTMLDivElement>;
  requestService: any;
  showArrows = true;
  modalOpen = false;
  id:any = 0
  idp: any = 0
  breakpointsTips = {
    550: {perView:1},
    991: { perView: 2 },
    1000: { perView: 3 },
    1200: { perView: 3 },
  };

 constructor(private requests : RequestService, private router:Router,  private formBuilder: FormBuilder) {
 this.requests.disparadorinfo.subscribe(data => {
  this.data = data

 })
 const iduser = localStorage.getItem('id');
 this.id = iduser
 console.log(this.id)
 this.loadExtraInfo()
 }



 async loadExtraInfo() {
  this.loadingExtraInfo = true;
  try {
    const resp = await firstValueFrom(
      forkJoin([
        this.requests.get('users/'),
        this.requests.getpost('comments/', { postId:this.idp, page_size: 3})
      ])
    );
    this.lastestTips = resp[0];
    this.lastcomment = resp[1];
    console.log(this.lastcomment)
    this.lastestTips.forEach((element: { storyIndex: any; }, index: any) => { element.storyIndex = index });
    console.log(this.lastestTips)
    this.tipsQty = this.lastestTips.length;
    this.loadingExtraInfo = false;
  } catch (err) { }

}



opencarrusel(id: any) {
  
  this.router.navigate(['dashboard/album/postalbum/',id ])
}

nextTip() {
  this.currentTip = this.lastestTips[this.currentTip.storyIndex + 1];
}

lastTip() {
  this.currentTip = this.lastestTips[this.currentTip.storyIndex - 1];
}
openAllTips() {

  this.router.navigate(['/basics/tips']);
}
  

openmodal(id:any){
     this.idp = id
     console.log(this.idp)
     this.loadExtraInfo()
}

recargarcarrusel(){
  this.loadExtraInfo()
}


}
