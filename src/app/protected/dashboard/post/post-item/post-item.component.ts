import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from 'src/app/servicios/post.service';
import { RequestService } from 'src/app/servicios/request.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.css']
})
export class PostItemComponent {
  isLiking = false;
  loadingComments = false;
  hasMoreComments = false;
  isReporting = false;
  isBlocking = false;
  isAddingToFavorites = false;
  isDeleting = false;
  canReply = false;
  user: any;
  @Input()
  post: any;
  @Input()
  favoritesQty = -1;
  @Input()
  showActions = true;
  @Input()
  showGoToDetails = false;
  @Input()
  community = true;
  postComments: any;
  currentCommentPage = 1;
  
  constructor(
    private requestService: RequestService,
    private servicio: PostService,
    private router: Router
  ) {
    console.log(this.post)
  }

  
  ngOnInit(): void {
    console.log(this.post)
  }

  


}
