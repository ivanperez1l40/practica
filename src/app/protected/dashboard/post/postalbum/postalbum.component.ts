import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { firstValueFrom, forkJoin } from 'rxjs';
import { RequestService } from 'src/app/servicios/request.service';

@Component({
  selector: 'app-postalbum',
  templateUrl: './postalbum.component.html',
  styleUrls: ['./postalbum.component.css']
})
export class PostalbumComponent {
  currentTip!: any
  lastestTips: any; 
  @Input()
  item: any
  data: any
  tipsQty: number | undefined;
  loadingExtraInfo = false;
  requestService: any;
  modalinfo: any;
  showArrows = true;
  modalOpen = false;
  breakpointsTips = {
    550: {perView:1},
    991: { perView: 2 },
    1000: { perView: 3 },
    1200: { perView: 3 },
  };
ida: any
 constructor(private requests : RequestService, private router:Router, private activeroute: ActivatedRoute) {
 this.ida = this.activeroute.snapshot.paramMap.get('id')
 console.log(this.ida)
 this.requests.disparadorinfo.emit( this.ida )

 this.loadExtraInfo()
 }

 async loadExtraInfo() {
  this.loadingExtraInfo = true;
  try {
    const resp = await firstValueFrom(
      forkJoin([
        this.requests.getalbum('album/')
      ])
    );
    console.log(resp[0])
    this.lastestTips = resp[0];
    this.lastestTips.forEach((element: { storyIndex: any; }, index: any) => { element.storyIndex = index });
    this.tipsQty = this.lastestTips.length;
    this.loadingExtraInfo = false;
  } catch (err) { }

}

openTipsModal(tip: any) {
  this.currentTip = tip;
  this.modalOpen = true;
  // this.modalinfo.show();
  this.router.navigate([`dashboard/album/${this.ida}/fotos`])
}

nextTip() {
  this.currentTip = this.lastestTips[this.currentTip.storyIndex + 1];
}

lastTip() {
  this.currentTip = this.lastestTips[this.currentTip.storyIndex - 1];
}
openAllTips() {
  this.modalinfo.hide();
  this.router.navigate(['/basics/tips']);
}
  
}
