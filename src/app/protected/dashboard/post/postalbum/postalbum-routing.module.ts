import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostalbumComponent } from './postalbum.component';
import { PostalbumfotosComponent } from './postalbumfotos/postalbumfotos.component';

const routes: Routes = [
  {
    path:'postalbum/:id', component:PostalbumComponent,
   
  },
  {
    path:':id/fotos',  component:PostalbumfotosComponent 
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostalbumRoutingModule { }
