import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostalbumRoutingModule } from './postalbum-routing.module';
import { PostalbumComponent } from './postalbum.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PostalbumfotosComponent } from './postalbumfotos/postalbumfotos.component';



@NgModule({
  declarations: [
    PostalbumComponent,
    PostalbumfotosComponent
  ],
  imports: [
    CommonModule,
    PostalbumRoutingModule,
    SharedModule
  ]
})
export class PostalbumModule { }
