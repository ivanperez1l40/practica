import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostalbumComponent } from './postalbum.component';

describe('PostalbumComponent', () => {
  let component: PostalbumComponent;
  let fixture: ComponentFixture<PostalbumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostalbumComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PostalbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
