import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostalbumfotosComponent } from './postalbumfotos.component';

describe('PostalbumfotosComponent', () => {
  let component: PostalbumfotosComponent;
  let fixture: ComponentFixture<PostalbumfotosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostalbumfotosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PostalbumfotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
