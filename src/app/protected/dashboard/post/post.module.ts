import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostRoutingModule } from './post-routing.module';
import { PostComponent } from './post.component';
import { PostItemComponent } from './post-item/post-item.component';
import { PostItemModule } from './post-item/post-item.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PostalbumComponent } from './postalbum/postalbum.component';




@NgModule({
  declarations: [
    PostComponent,
    PostalbumComponent,
    
  ],
  imports: [
    CommonModule,
    PostRoutingModule,
    PostItemModule,
    SharedModule

   
  ]
 
})
export class PostModule { }
