import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PostComponent } from './dashboard/post/post.component';



const routes: Routes = [
  
    {path:'', component:DashboardComponent,
    children: [
      {path: 'post', component: PostComponent },
      {
        path: 'album',
        loadChildren: () => import('./dashboard/post/postalbum/postalbum.module').then(m => m.PostalbumModule)
      },
      
      {path :  '**', redirectTo: 'post'}
    ]},
    
   
    
  
  

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtectedRoutingModule { }
