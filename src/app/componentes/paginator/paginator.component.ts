import { firstValueFrom } from 'rxjs';
import {
  Component,
  ContentChild,
  OnInit,
  TemplateRef,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  ElementRef,
} from '@angular/core';
import { RequestService } from 'src/app/servicios/request.service';
@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit, OnChanges {
  @ContentChild(TemplateRef) templateRef!: TemplateRef<any>;

  @Input()
  showPagination = true;

  @Input()
  endpoint = '';

  @Input()
  classContent = 'row h-100';

  @Input()
  queryParams = {};

  @Input()
  pageSize = 10;

  @Input()
  area:any

  @Input()
  canScrollTo = true;

  @Output()
  onLoaded: EventEmitter<any> = new EventEmitter<any>();

  page = 1;
  totalPages = 0;

  info: any;

  isLoading = false;
  id: any;
  constructor(private request: RequestService, private el: ElementRef) {
    const iduser = localStorage.getItem('id');
    this.id = iduser
   }

  async ngOnInit(): Promise<void> {
    if (!Object.keys(this.queryParams).length) {
      await this.getInfo();
    }
  }

  async getInfo() {
    this.isLoading = true;
    try {
      this.info = await firstValueFrom(
        this.request.getpost(this.endpoint, {
          userId:this.id ,
          page: this.page,
          page_size: this.pageSize,
        })
        
      );
   
       console.log(this.info)
      this.totalPages = this.info?.length
        ? Math.ceil(this.info?.length / this.pageSize)
        : 0;
    } catch (err) {
      this.totalPages = 0;
    }

    this.onLoaded.emit();
    this.isLoading = false;

    if (this.canScrollTo) {
      this.el.nativeElement.scrollIntoView();
    }
  }

  reset() {
    this.info = null;
    this.page = 1;
    this.totalPages = 0;
  }

  async previous() {
    if (!this.info.previous) {
      return;
    }

    if (this.page - 1 < 1) {
      return;
    }

    this.page -= 1;

    await this.getInfo();
  }

  async next() {
    if (!this.info.next) {
      return;
    }

    this.page += 1;

    await this.getInfo();
  }

  get pagesItems(): Array<number> {
    if (this.info?.results?.length) {
      const maxItems = this.totalPages < 5 ? this.totalPages : 5;

      return Array(maxItems)
        .fill('')
        .map((_, item) => {
          let increment = 0;

          if (this.page > 3) {
            increment = this.page - 3;
          }

          if (this.page >= this.totalPages - 2) {
            increment = this.totalPages - maxItems;
          }

          return item + 1 + increment;
        });
    }

    return [];
  }


  async goTo(page: number) {
    if (this.page == page) {
      return;
    }
    this.page = page;
    await this.getInfo();
  }

  ngOnChanges(changes: SimpleChanges): void {

    if ('queryParams' in changes) {
      this.reset();
      this.getInfo();
    }
  }
}
