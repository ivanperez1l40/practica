import { environment } from '../enviroments/enviroment';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
// import { SessionService } from '@services/session.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor() { }
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let url = '';
    let headers = {} as any;
      if (req.url.startsWith('http') || req.url.startsWith('/assets') || req.url.includes('giphy')) {
      url = req.url;
    } else {
      url = environment.baseUrl;
      url = `${url}${req.url}`;
      console.log(url)
    
    }

    const request = req.clone({
      setHeaders: {
        ...headers,
      },
      url,
    });

    return next.handle(request);
  }
}
